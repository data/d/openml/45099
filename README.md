# OpenML dataset: Prostate

https://www.openml.org/d/45099

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Prostate dataset, 102 intances**

**Authors**: D. Singh, P. Febbo, K. Ross, D. Jackson, J. Manola, C. Ladd, P. Tamayo, A. Renshaw, A. D'Amico, J. Richie, et al

**Please cite**: ([URL](https://www.sciencedirect.com/science/article/pii/S1535610802000302)): D. Singh, P. Febbo, K. Ross, D. Jackson, J. Manola, C. Ladd, P. Tamayo, A. Renshaw, A. D'Amico, J. Richie, et al, Gene expression correlates of clinical prostate cancer behavior, Cancer Cell 1 (2) (2002) 203-209.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45099) of an [OpenML dataset](https://www.openml.org/d/45099). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45099/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45099/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45099/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

